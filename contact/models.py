from enum import unique
from pyexpat import model
from django.db import models

class Contact (models.Model):
    first_name=models.CharField(max_length=50)
    last_name=models.CharField(max_length=50)
    dni=models.CharField(max_length=10, unique=True)
    cellphone=models.CharField(max_length=10, blank=False, null=False, unique=True)
    email=models.EmailField(max_length=255, unique=True)
    class Meta:
        db_table='contact'

    def __str__(self):
        return u'%s %s' % (self.first_name, self.last_name)
 

