from django.urls import path
from contact.views import del_contact, list_contact, save_contact, edit_contact, del_contact
urlpatterns = [
    path('', list_contact),
    path('save-contact', save_contact),
    path('edit-contact', edit_contact),
    path('delete-contact', del_contact),
]
