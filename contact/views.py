from django.shortcuts import redirect, render
from django.http import HttpResponse
from .models import Contact
from django.contrib import messages


def list_contact(request, *args, **kwards):
    if request.method == 'GET':
        allContacts = Contact.objects.all().values()
        context = {
            'allContacts': allContacts
        }
        return render(request, 'contact_list.html', context)


def save_contact(request, *args, **kwards):
    if request.method == 'POST':
        contact = Contact()
        contact.first_name = request.POST.get('first_name')
        contact.last_name = request.POST.get('last_name')
        contact.dni = request.POST.get('dni')
        contact.cellphone = request.POST.get('cellphone')
        contact.email = request.POST.get('email')
        if contact.save() != True:
            messages.add_message(request, messages.SUCCESS,
                                 "Contacto creado con exito", fail_silently=True)
            return redirect(list_contact)
        else:
            messages.add_message(request, messages.ERROR,
                                 "El contacto no se pudo crear", fail_silently=True)


def edit_contact(request, *args, **kwards):
    if request.method == 'POST':
        contact = Contact.objects.get(dni=request.POST['dni'])
        contact.first_name = request.POST.get('first_name')
        contact.last_name = request.POST.get('last_name')
        contact.dni = request.POST.get('dni')
        contact.cellphone = request.POST.get('cellphone')
        contact.email = request.POST.get('email')
        if contact.save() != True:
            messages.add_message(request, messages.SUCCESS,
                                 "El contacto se edito con exito", fail_silently=True)
            return redirect(list_contact)
        else:
            messages.add_message(request, messages.ERROR,
                                 "El contacto no se pudo editar", fail_silently=True)


def del_contact(request, *args, **kwards):
    if request.method == 'GET':
        contact = Contact.objects.get(dni=request.GET['dni'])
        if contact.delete():
            messages.add_message(request, messages.SUCCESS,
                                 "Se ha borrado con exito", fail_silently=True)
        else:
            messages.add_message(
                request, messages.ERROR, "No se ha borrado con exito", fail_silently=True)
        return redirect(list_contact)
