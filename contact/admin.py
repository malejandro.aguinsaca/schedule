from django.contrib import admin
from .models import Contact

class AdminContact(admin.ModelAdmin):
    list_display=['__str__', 'first_name', 'last_name', 'dni', 'cellphone', 'email']
    class Meta(object):
        model=Contact

def __str__(self):
    return self.dni


admin.site.register(Contact, AdminContact)