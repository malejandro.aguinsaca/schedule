from django.urls import path
from note.views import list_notes, save_note, edit_note, del_note, filter_notes
urlpatterns = [
    path('', list_notes),
    path('filter-notes', filter_notes),
    path('save-note', save_note),
    path('edit-note', edit_note),
    path('delete-note', del_note),
]
