from django.contrib import admin
from .models import Note

class AdminNote(admin.ModelAdmin):
    list_display=['__str__', 'title', 'content']
    class Meta(object):
        model=Note

admin.site.register(Note, AdminNote)