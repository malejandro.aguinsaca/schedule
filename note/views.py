from django.shortcuts import render, redirect
from contact.models import Contact
from .models import Note
from django.db.models import F
from django.contrib import messages


def list_notes(request, *args, **kwards):
    if request.method == 'GET':
        allNotes = Note.objects.all().values('id', 'title', 'content', first_name=F(
            'contact__first_name'), last_name=F('contact__last_name'), dni=F('contact__dni'))
        allContacts = Contact.objects.all().values()
        contactsJSON = []
        for contact in allContacts:
            contactsJSON.append({'dni': contact.get('dni'), 'first_name': contact.get(
                'first_name'), 'last_name': contact.get('last_name')})
        context = {
            'allNotes': allNotes,
            'allContacts': contactsJSON
        }

        return render(request, 'note.html', context)


def filter_notes(request, *args, **kwards):
    if request.method == 'GET':
        allNotes = Note.objects.filter(contact__dni=request.GET.get('dni')).values('id', 'title', 'content', first_name=F(
            'contact__first_name'), last_name=F('contact__last_name'), dni=F('contact__dni'))
        allContacts = Contact.objects.all().values()
        contactsJSON = []
        for contact in allContacts:
            contactsJSON.append({'dni': contact.get('dni'), 'first_name': contact.get(
                'first_name'), 'last_name': contact.get('last_name')})
        context = {
            'allNotes': allNotes,
            'allContacts': contactsJSON
        }

        return render(request, 'note.html', context)


def save_note(request, *args, **kwards):
    if request.method == 'POST':
        contact = Contact.objects.get(dni=request.POST.get('dni'))
        note = Note()
        note.title = request.POST.get('title')
        note.content = request.POST.get('content')
        note.contact = contact
        if note.save() != True:
            messages.add_message(request, messages.SUCCESS,
                                 "Nota creada con exito", fail_silently=True)
            return redirect(list_notes)
        else:
            messages.add_message(request, messages.ERROR,
                                 "No se pudo crear la Nota", fail_silently=True)
            return redirect(list_notes)


def edit_note(request, *args, **kwards):
    if request.method == 'POST':
        contact = Contact.objects.get(dni=request.POST.get('dni'))
        note = Note.objects.get(pk=request.POST['id'])
        note.title = request.POST.get('title')
        note.content = request.POST.get('content')
        note.contact = contact
        if note.save() != True:
            messages.add_message(request, messages.SUCCESS,
                                 "Nota editada con exito", fail_silently=True)
            return redirect(list_notes)
        else:
            messages.add_message(request, messages.ERROR,
                                 "No se pudo editar la Nota", fail_silently=True)
            return redirect(list_notes)


def del_note(request, *args, **kwards):
    if request.method == 'GET':
        note = Note.objects.get(pk=request.GET['id'])
        if note.delete():
            messages.add_message(request, messages.SUCCESS,
                                 "Se ha borrado con exito", fail_silently=True)
        else:
            messages.add_message(
                request, messages.ERROR, "No se ha borrado con exito", fail_silently=True)
        return redirect(list_notes)
