from django.db import models
from contact.models import Contact

class Note(models.Model):
    title=models.CharField(max_length=30)
    content=models.TextField()
    contact=models.ForeignKey(Contact, on_delete=models.CASCADE, related_name="contact")
    class Meta:
        db_table='note'
    def __str__(self):
        return u'%s -> %s' % (self.title, self.contact.first_name)

